package com.example.data.mapper

import com.example.data.entities.CalculatorApiData
import com.example.data.entities.CalculatorDBData
import com.example.domain.model.Computation
import com.example.domain.model.ComputationType
import com.example.domain.model.Computer

class ComputerEntityMapper {

    fun mapToEntity(data: CalculatorApiData): Computer = Computer(
        history = mapListComputationsToEntity(data.history)
    )

    fun mapToEntity(data: List<CalculatorDBData>): Computer = Computer(
        history = mapListComputationsToEntity(data)
    )


    fun mapListComputationsToEntity(history: List<CalculatorDBData>)
            : List<Computation> = history?.map { mapComputationToEntity(it) }

    fun mapComputationToEntity(computation: CalculatorDBData): Computation = Computation(
        id = computation.id,
        firstOperand = computation.firstOperand ?: 0.0,
        secondOperand = computation.secondOperand  ?: 0.0,
        operator = mapOperatorToComputationType(computation.operator)
    )

    fun mapOperatorToComputationType(operator : String?) : ComputationType = when (operator) {
        "+" -> ComputationType.ADDITION
        "-" -> ComputationType.SUBTRACTION
        "/" -> ComputationType.DIVISION
        "*" -> ComputationType.MULTIPLICATION
        else -> ComputationType.NONE
    }

}