package com.example.data.repository

import com.example.data.api.CalculatorApi
import com.example.data.mapper.ComputerEntityMapper
import com.example.domain.model.Computer
import io.reactivex.Single

class CalculatorApiHistory (private val api: CalculatorApi,
                            private val historyMapper : ComputerEntityMapper
): ICalculatorHistory {

    override fun getHistory(): Single<Computer> {
        return api.getCalculatorHistory().map {
            historyMapper.mapToEntity(it)
        }
    }
}