package com.example.data.repository

import com.example.data.database.CalculatorDAO
import com.example.data.database.CalculatorDB
import com.example.data.mapper.ComputerEntityMapper
import com.example.domain.model.Computer
import io.reactivex.Single

class CalculatorLocalHistory(private val database: CalculatorDB,
                    private val mapper: ComputerEntityMapper) : ICalculatorHistory {

    private val calculatorDAO : CalculatorDAO  = database.calculatorDAO()

    override fun getHistory(): Single<Computer> {
        return calculatorDAO.getAllComputations().map {
            mapper.mapToEntity(it)
        }
    }
}