package com.example.data.repository

import com.example.domain.model.Computer
import io.reactivex.Single

interface ICalculatorHistory{
    fun getHistory(): Single<Computer>
}