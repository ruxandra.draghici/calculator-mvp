package com.example.data.repository

import com.example.domain.model.Computation
import com.example.domain.model.Computer
import com.example.domain.repository.CalculatorRepository
import io.reactivex.Flowable
import io.reactivex.Single

class CalculatorRepositoryImpl(private val apiHistory: CalculatorApiHistory,
                         private val localHistory: CalculatorLocalHistory) : CalculatorRepository {
    override fun getCalculatorHistoryLocal(): Single<Computer> {
        return localHistory.getHistory()
    }

    override fun getCalculatorHistoryApi(): Single<Computer> {
        return apiHistory.getHistory()
    }

    override fun getCalculatorHistory(): Flowable<Computer> {
        return getCalculatorHistoryApi()
            .mergeWith(getCalculatorHistoryLocal())
    }

    override fun requestNewComputation(computation: Computation): Double {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


}