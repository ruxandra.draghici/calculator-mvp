package com.example.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.data.entities.CalculatorDBData

@Database(entities = arrayOf(CalculatorDBData::class), version = 1)
abstract class CalculatorDB : RoomDatabase() {
    abstract fun calculatorDAO(): CalculatorDAO
}