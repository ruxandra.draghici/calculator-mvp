package com.example.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.data.entities.CalculatorDBData
import io.reactivex.Single

@Dao
interface CalculatorDAO{

    @Query("SELECT * FROM calculator_data")
    fun getAllComputations(): Single<List<CalculatorDBData>>

    @Query("SELECT * FROM calculator_data WHERE id = :id")
    fun getComputationById(id : Int): CalculatorDBData

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAllComputations(computations : List<CalculatorDBData>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveNewComputation(computation : CalculatorDBData)

    @Query("DELETE FROM calculator_data")
    fun clear()

}