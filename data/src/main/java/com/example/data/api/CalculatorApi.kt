package com.example.data.api

import com.example.data.Utils.Constants
import com.example.data.entities.CalculatorApiData
import io.reactivex.Single
import retrofit2.Call
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET


interface CalculatorApi {

    @GET("/history")
    fun getCalculatorHistory() : Single<CalculatorApiData>

    companion object Factory {
        fun create(): CalculatorApi {
            val retrofit = retrofit2.Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Constants.BASE_URL)
                .build()

            return retrofit.create(CalculatorApi::class.java)
        }
    }
}