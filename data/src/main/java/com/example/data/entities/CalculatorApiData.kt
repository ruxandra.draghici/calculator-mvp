package com.example.data.entities

import com.google.gson.annotations.SerializedName

class CalculatorApiData(
    @SerializedName("history")
    var history: List<CalculatorDBData> = emptyList()
)