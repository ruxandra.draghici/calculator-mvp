package com.example.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "calculator_data")
data class CalculatorDBData(
    @PrimaryKey(autoGenerate = true) var id: Int = 0,
    var firstOperand: Double? = null,
    var secondOperand: Double? = null,
    var operator: String? = null)