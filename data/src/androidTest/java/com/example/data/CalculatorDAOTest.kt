package com.example.data

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.data.database.CalculatorDAO
import com.example.data.database.CalculatorDB
import com.example.data.entities.CalculatorDBData
import junit.framework.Assert.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith



@RunWith(AndroidJUnit4::class)
class CalculatorDAOTest {
    lateinit var calculatorDAO: CalculatorDAO
    lateinit var database: CalculatorDB

    @Before
    fun setup() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        database = Room.inMemoryDatabaseBuilder(context, CalculatorDB::class.java).build()
        calculatorDAO = database.calculatorDAO()
    }

    @After
    fun tearDown() {
        database.close()
    }

    @Test
    fun testInsertedAndRetrievedSeveralComputationsMatch() {
        val computations = listOf(
            CalculatorDBData(1, 8.0, 10.0, "+"),
            CalculatorDBData(2, 9.0, 19.0, "-")
        )
        calculatorDAO.saveAllComputations(computations)

        val allComputations = calculatorDAO.getAllComputations()
        assertEquals(computations, allComputations)
    }

    @Test
    fun testConflictingInsertsReplaceComputations() {
        val computations = listOf(
            CalculatorDBData(1, 7.0, 100.0, "/"),
            CalculatorDBData(2, 8.0, 101.0, "*"),
            CalculatorDBData(3, 9.0, 102.0, "+")
        )
        calculatorDAO.saveAllComputations(computations)

        val computations2 = listOf(
            CalculatorDBData(1, 10.0, 103.0, "/"),
            CalculatorDBData(2, 11.0, 104.0, "/"),
            CalculatorDBData(4, 12.0, 105.0, "/")
        )
        calculatorDAO.saveAllComputations(computations2)

        val allComputations = calculatorDAO.getAllComputations()
        val expectedComputations = listOf(
            CalculatorDBData(1, 10.0, 103.0, "/"),
            CalculatorDBData(2, 11.0, 104.0, "/"),
            CalculatorDBData(3, 9.0, 102.0, "+"),
            CalculatorDBData(4, 12.0, 105.0, "/")
        )

        assertEquals(expectedComputations, allComputations)
    }

    @Test
    fun testInsertedAndRetrievedOneComputationMatch() {
        val computation = CalculatorDBData(1, 8.0, 10.0, "+")
        calculatorDAO.saveNewComputation(computation)

        val expectedComputation = calculatorDAO.getComputationById(1)

        assertEquals(computation, expectedComputation)
    }

}