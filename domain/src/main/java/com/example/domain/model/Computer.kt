package com.example.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Computer(
    val history : List<Computation>
) : Parcelable {

    fun List<Computation>.isInList(computation : Computation) : Boolean =  this.contains(computation)
}
