package com.example.domain.model

enum class ComputationType{
    ADDITION,
    MULTIPLICATION,
    DIVISION,
    SUBTRACTION,
    NONE
}