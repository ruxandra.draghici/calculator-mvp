package com.example.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Computation(
    val id : Int,
    val firstOperand : Double,
    val secondOperand : Double,
    val operator : ComputationType
) : Parcelable
