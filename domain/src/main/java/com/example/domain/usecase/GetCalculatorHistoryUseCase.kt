package com.example.domain.usecase

import com.example.domain.model.Computer
import com.example.domain.repository.CalculatorRepository
import io.reactivex.Flowable
import io.reactivex.Single

class GetCalculatorHistoryUseCase(private val calculatorRepository : CalculatorRepository) {

    operator fun invoke() : Flowable<Computer> = calculatorRepository.getCalculatorHistory()
}