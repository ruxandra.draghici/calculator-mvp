package com.example.domain.usecase

import com.example.domain.model.Computation
import com.example.domain.repository.CalculatorRepository


class RequestNewComputationUseCase(private val calculatorRepository : CalculatorRepository) {

    operator fun invoke(computation : Computation) : Double = calculatorRepository.requestNewComputation(computation)
}