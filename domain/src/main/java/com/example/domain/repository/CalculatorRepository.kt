package com.example.domain.repository

import com.example.domain.model.Computation
import com.example.domain.model.Computer
import io.reactivex.Flowable
import io.reactivex.Single

interface CalculatorRepository {
    fun getCalculatorHistoryLocal(): Single<Computer>
    fun getCalculatorHistoryApi(): Single<Computer>
    fun getCalculatorHistory(): Flowable<Computer>
    fun requestNewComputation(computation : Computation) : Double
}