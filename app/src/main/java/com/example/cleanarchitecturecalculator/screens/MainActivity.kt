package com.example.cleanarchitecturecalculator.screens

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.room.Room
import com.example.cleanarchitecturecalculator.R
import com.example.cleanarchitecturecalculator.mvp.presenter.CalculatorPresenter
import com.example.data.api.CalculatorApi
import com.example.data.database.CalculatorDB
import com.example.data.mapper.ComputerEntityMapper
import com.example.data.repository.CalculatorApiHistory
import com.example.data.repository.CalculatorLocalHistory
import com.example.data.repository.CalculatorRepositoryImpl
import com.example.domain.model.ComputationType
import com.example.domain.usecase.GetCalculatorHistoryUseCase
import com.example.domain.usecase.RequestNewComputationUseCase
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),
    CalculatorContract.View, View.OnClickListener {
    //also can implement it with lateinit
    private lateinit var presenter: CalculatorPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val db = Room.databaseBuilder(
            applicationContext,
            CalculatorDB::class.java, "calculator-history"
        ).build()

        val computationRepository =
            CalculatorRepositoryImpl(CalculatorApiHistory(CalculatorApi.create(), ComputerEntityMapper()),
                CalculatorLocalHistory(db, ComputerEntityMapper())
            )
        presenter =
            CalculatorPresenter(
                this,
                RequestNewComputationUseCase(computationRepository),
                GetCalculatorHistoryUseCase(computationRepository)
            )

        setUpListeners()
    }

    private fun setUpListeners() {
        btn_0.setOnClickListener(this)
        btn_1.setOnClickListener(this)
        btn_2.setOnClickListener(this)
        btn_3.setOnClickListener(this)
        btn_4.setOnClickListener(this)
        btn_5.setOnClickListener(this)
        btn_6.setOnClickListener(this)
        btn_7.setOnClickListener(this)
        btn_8.setOnClickListener(this)
        btn_9.setOnClickListener(this)
        btn_plus.setOnClickListener(this)
        btn_divide.setOnClickListener(this)
        btn_minus.setOnClickListener(this)
        btn_multiply.setOnClickListener(this)
        btn_equals.setOnClickListener(this)
        btn_clear.setOnClickListener(this)
    }


    override fun showResult(value: Double) {
        operator.text = ""
        first_operand.text = value.toString()
        second_operand.text = ""

    }

    override fun getFirstOperand(): Double = if (!first_operand.text.isNullOrEmpty())
        first_operand.text.toString().toDouble()
        else 0.0

    override fun getSecondOperand(): Double = if (!second_operand.text.isNullOrEmpty())
            second_operand.text.toString().toDouble() else 0.0

    override fun getOperator(): ComputationType = when (operator.text.toString()) {
        "+" -> ComputationType.ADDITION
        "-" -> ComputationType.SUBTRACTION
        "/" -> ComputationType.DIVISION
        "*" -> ComputationType.MULTIPLICATION
        else -> ComputationType.NONE
    }

    override fun displayFirstOperand(value: Double) {
        first_operand.text = value.toString()
    }

//    have an event on the viewmodel that the view would subscribe to when a dialog was required.

    override fun displaySecondOperand(value: Double) {
        second_operand.text = value.toString()
    }

    override fun displayOperator(value: ComputationType) {
        operator.text = when (value) {
            ComputationType.ADDITION -> "+"
            ComputationType.MULTIPLICATION -> "*"
            ComputationType.DIVISION -> "/"
            ComputationType.SUBTRACTION -> "-"
            ComputationType.NONE -> ""
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            btn_equals.id -> presenter.onComputationRequest()
            btn_clear.id -> presenter.onClearPress()
            btn_plus.id -> presenter.onOperatorPress(ComputationType.ADDITION)
            btn_minus.id -> presenter.onOperatorPress(ComputationType.SUBTRACTION)
            btn_multiply.id -> presenter.onOperatorPress(ComputationType.MULTIPLICATION)
            btn_divide.id -> presenter.onOperatorPress(ComputationType.DIVISION)
            btn_0.id -> presenter.onNumberPress(0.0)
            btn_1.id -> presenter.onNumberPress(1.0)
            btn_2.id -> presenter.onNumberPress(2.0)
            btn_3.id -> presenter.onNumberPress(3.0)
            btn_4.id -> presenter.onNumberPress(4.0)
            btn_5.id -> presenter.onNumberPress(5.0)
            btn_6.id -> presenter.onNumberPress(6.0)
            btn_7.id -> presenter.onNumberPress(7.0)
            btn_8.id -> presenter.onNumberPress(8.0)
            btn_9.id -> presenter.onNumberPress(9.0)
        }
    }
}
