package com.example.cleanarchitecturecalculator.mvp.presenter

import com.example.cleanarchitecturecalculator.utils.addNewDigit
import com.example.domain.model.ComputationType
import com.example.domain.usecase.GetCalculatorHistoryUseCase
import com.example.domain.usecase.RequestNewComputationUseCase

class CalculatorPresenter(
    private var view: CalculatorContract.View,
    private val requestNewComputation: RequestNewComputationUseCase,
    private val historyUseCase: GetCalculatorHistoryUseCase
) : CalculatorContract.Presenter{


    fun onComputationRequest() {
        //TODO: add logic for the db/api version

//        val result: Double = requestNewComputation.invoke(
//            Computation(
//                view.getFirstOperand(),
//                view.getSecondOperand(),
//                view.getOperator()
//            )
//        )
//        view.showResult(result)
    }

    fun onOperatorPress(operator: ComputationType) {
        view.displayOperator(operator)

    }

    fun onNumberPress(value: Double) {
        if (view.getOperator() == ComputationType.NONE) view.displayFirstOperand(
            addNewDigit(
                view.getFirstOperand(),
                value
            )
        )
        else
            view.displaySecondOperand(
                addNewDigit(
                    view.getSecondOperand(),
                    value
                )
            )
    }

    fun onClearPress() {
        view.displayOperator(ComputationType.NONE)
        view.displayFirstOperand(0.0)
        view.displaySecondOperand(0.0)
    }

    fun onHistoryPress () {
        //TODO: add UI interaction with history
    }


}