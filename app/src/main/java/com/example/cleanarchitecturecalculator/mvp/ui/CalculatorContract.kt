
import com.example.domain.model.ComputationType

class CalculatorContract {

    interface View {

        fun showResult(value: Double)
        fun getFirstOperand() : Double
        fun getSecondOperand() : Double
        fun getOperator() : ComputationType
        fun displayFirstOperand(value : Double)
        fun displaySecondOperand(value : Double)
        fun displayOperator(value :ComputationType)
    }

    interface Presenter {

    }
}
