package com.example.cleanarchitecturecalculator.utils


fun addition(operand1: Double, operand2: Double): Double = operand1 + operand2

fun subtraction(operand1: Double, operand2: Double): Double = operand1 - operand2

fun division(operand1: Double, operand2: Double): Double = operand1 / operand2

fun multiplication(operand1: Double, operand2: Double): Double = operand1 * operand2

fun addNewDigit(number: Double, newDigit: Double): Double = number * 10 + newDigit